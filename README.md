# Portable Symmetric Key Container Crypto Library

The Java library to encrypt/decrypt PSKC XML files (RFC-6030)

TODO:

* Encrypt PSKC Documents
* Sign PSKC Documents

v0.1.0 Features:

* Adopted design to resemble <code>javax.crypto.Cipher</code>
* Decrypts PSKC Document instances
* Validates Signature

Usage Example:

```
//Parse document
final DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
dbFactory.setNamespaceAware(true);
Document doc = dbFactory.newDocumentBuilder().parse(fXmlFile);

//Decrypt and validate signature of document
final PskcCipher cipher = PskcCipher.getInstance();
cipher.init(PskcCipher.DECRYPT_MODE, "Paasswooooord".getBytes());
doc = cipher.doFinal(doc);
);
```
