/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.pskccrypt;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import org.apache.xml.security.algorithms.JCEMapper;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.engines.AESFastEngine;
import org.bouncycastle.crypto.engines.DESedeWrapEngine;
import org.bouncycastle.crypto.engines.RFC3394WrapEngine;
import org.bouncycastle.crypto.params.KeyParameter;

/**
 * Crypto utilities used in RFC6030 implementation.
 *
 * @author Middleware Services
 */
public final class Crypto
{

  /**
   * Private constructor of utility class.
   */
  private Crypto()
  {

  }

  /**
   * Unwraps a given cipher data as specified by RFC3394 (AES). Keys must be multiples of 64 bits.
   *
   * @param kek Key encrypting key
   * @param keyData Cipher data
   * @return Unwrapped key
   * @throws InvalidCipherTextException Incase of crypto exceptions
   */
  public static byte[] unwrapAESBlockLengthKey(final byte[] kek, final byte[] keyData) throws InvalidCipherTextException
  {
    final RFC3394WrapEngine engine = new RFC3394WrapEngine(new AESFastEngine());
    engine.init(false, new KeyParameter(kek));
    return engine.unwrap(keyData, 0, keyData.length);
  }

  /**
   * Unwraps a given cipher data as specified by RFC3217 (3DES).
   *
   * @param kek Key encrypting key
   * @param keyData Cipher data
   * @return Unwrapped key
   * @throws InvalidCipherTextException Incase of crypto exceptions
   */
  public static byte[] unwrapDESedeKey(final byte[] kek, final byte[] keyData) throws InvalidCipherTextException
  {
    final DESedeWrapEngine engine = new DESedeWrapEngine();
    engine.init(false, new KeyParameter(kek));
    return engine.unwrap(keyData, 0, keyData.length);
  }

  /**
   * Casts a 3DES key with any key option to 3TDEA as required by DESede algorithms.
   *
   * @param keyData 3DES key to ensure length integrity for
   * @return 24 octet 3DES key
   */
  public static byte[] cast3DESnTDEATo3TDEA(final byte[] keyData)
  {
    final byte[] fullDESedeKey = new byte[24];
    switch (keyData.length) {
    case 24:
      return keyData;
    case 16:
      System.arraycopy(keyData, 0, fullDESedeKey, 0, keyData.length);
      System.arraycopy(keyData, 0, fullDESedeKey, keyData.length, 8);
      break;
    case 8:
      System.arraycopy(keyData, 0, fullDESedeKey, 0, 8);
      System.arraycopy(keyData, 0, fullDESedeKey, 8, 8);
      System.arraycopy(keyData, 0, fullDESedeKey, 16, 8);
      break;
    default:
      throw new IllegalArgumentException("Invalid TripleDES key length");
    }
    return fullDESedeKey;
  }

  /**
   * Derives a key given a key encrypting key, pseudo random function algorithm (default HMACSHA1), salt, iteration, and
   * key length in octets using PKCS#5 RFC2898 Appendix-A.2.
   *
   * @param kek Key encrypting key
   * @param prfPublicAlgorithm JCE PRF <b>(may be null)</b> <i>defaults to: HMACSHA1</i>
   * @param salt Salt bytes
   * @param iteration Number of times to iterate
   * @param keyLengthOctets Key length in octets (common bytes)
   * @return Derived key bytes
   */
  public static byte[] derivePBKDF2KeyFromKEK(final byte[] kek, final String prfPublicAlgorithm,
          final byte[] salt, final int iteration, final int keyLengthOctets
  )
  {
    try {
      //PKCS5 Default PRF is HMACSHA1 as per RFC-2898 B.1.1
      String prf = Constants.DEFAULT_PBKDF2_PRF;
      if (prfPublicAlgorithm != null) {
        prf = JCEMapper.translateURItoJCEID(prfPublicAlgorithm);
        if (prf == null) {
          throw new PskcCryptoException("Unknown pseudo random function for PBKDF2 algorithm");
        }
      }
      final SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2With" + prf);
      /**
       * @TODO if the document is not signed, derive key length from salt *
       */
      final KeySpec spec = new PBEKeySpec(
              new String(kek, Constants.GLOBAL_ENCODING).toCharArray(), salt,
              iteration,
              keyLengthOctets * 8);
      return factory.generateSecret(spec).getEncoded();
    } catch (NoSuchAlgorithmException | UnsupportedEncodingException | InvalidKeySpecException ex) {
      throw new PskcCryptoException("Could not derive PBKDF2 key from key encrypting key", ex);
    }
  }

}
