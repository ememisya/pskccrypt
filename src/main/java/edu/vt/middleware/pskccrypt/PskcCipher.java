/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.pskccrypt;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.naming.OperationNotSupportedException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import edu.vt.middleware.pskccrypt.util.Jaxb;
import edu.vt.middleware.pskccrypt.util.Xml;
import ietf.params.xml.ns.keyprov.pskc.KeyContainer;
import org.apache.xml.security.algorithms.JCEMapper;
import org.apache.xml.security.encryption.XMLCipher;
import org.apache.xml.security.encryption.XMLEncryptionException;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.util.Arrays;
import org.w3._2001._04.xmlenc_.EncryptedData;
import org.w3._2009.xmlenc11_.DerivedKey;
import org.w3._2009.xmlenc11_.KeyDerivationMethod;
import org.w3._2009.xmlenc11_.PBKDF2Params;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * The crypto utility to perform encryption and decryption of PSKC XML files.
 *
 * @author Middleware Services
 */
public final class PskcCipher
{

  /**
   * ENCRYPT Mode constant
   */
  public static final int ENCRYPT_MODE = Cipher.ENCRYPT_MODE;

  /**
   * DECRYPT Mode constant
   */
  public static final int DECRYPT_MODE = Cipher.DECRYPT_MODE;

  /**
   * JAXB context instance
   */
  public static final JAXBContext JAXB_CONTEXT = initializeJaxbContext();

  /**
   * Logger instance
   */
  private static final org.slf4j.Logger LOGGER =
          org.slf4j.LoggerFactory.getLogger(PskcCipher.class);

  /**
   * Initialize <b>org.apache.santuario.xmlsec</b>, and jaxbContext
   */
  static
  {
    org.apache.xml.security.Init.init();
  }

  /**
   * Key data
   */
  protected byte[] key;

  /**
   * MAC Key data
   */
  protected byte[] macKey;

  /**
   * Underlying {@link XMLCipher} instance used for all xml security operations
   */
  protected final XMLCipher xmlCipher;

  /**
   * Document builder instance to be passed into this library
   */
  protected final DocumentBuilder documentBuilder;

  /**
   * Mode that the XMLCipher object is operating in
   */
  private int cipherMode = Integer.MIN_VALUE;

  /**
   * Private constructor of utility class.
   *
   * @throws XMLEncryptionException If <b>org.apache.santuario.xmlsec</b> cannot be initialized
   */
  private PskcCipher() throws XMLEncryptionException
  {
    try {
      documentBuilder = Xml.newNamespaceAwareDocumentBuilder();
    } catch (ParserConfigurationException ex) {
      LOGGER.error("Could not initialize documentBuilder", ex);
      throw new PskcCryptoException("Could not initialize cipher", ex);
    }
    xmlCipher = XMLCipher.getInstance();
  }

  /**
   * Returns an instance of this class.
   *
   * @return {@link PskcCipher} instance
   */
  public static PskcCipher getInstance()
  {
    try {
      return new PskcCipher();
    } catch (XMLEncryptionException ex) {
      LOGGER.error("Could not initalize cipher instance", ex);
      throw new IllegalStateException("Could not initialize instance (underlying dependencies failed to initalize)");
    }
  }

  /**
   * Initializes this cipher with a key or kek.
   * <p>
   * The cipher is initialized for one of the following four operations: encryption, decryption.
   *
   * @param opmode the operation mode of this cipher (this is one of the following: ENCRYPT_MODE, DECRYPT_MODE)
   * @param keyArgument The key or kek to be used with this cipher instance
   * @throws XMLEncryptionException If the underlying {@link XMLCipher} could not be initalized
   */
  public void init(final int opmode, final byte[] keyArgument) throws XMLEncryptionException
  {
    xmlCipher.init(opmode, null);
    switch (opmode) {
    case ENCRYPT_MODE:
      throw new UnsupportedOperationException("This mode has not been implemented yet.");
    case DECRYPT_MODE:
      break;
    default:
      throw new XMLEncryptionException("Invalid mode in init");
    }
    cipherMode = opmode;
    this.key = keyArgument;
  }

  /**
   * Process and validate the contents of a {@link Document}. The processing depends on the initialization parameters
   * of {@link #init(int, byte[]) init()}.
   *
   * @param contextDocument the context <code>Document</code>.
   * @return the processed <code>Document</code>.
   * @throws Exception to indicate any exceptional conditions.
   */
  public Document doFinal(final Document contextDocument)
          throws Exception
  {
    if (null == contextDocument || null == key || cipherMode == Integer.MIN_VALUE) {
      throw new IllegalArgumentException("Context document, mode or key cannot be null");
    }
    final KeyContainer unmarshalledDocument = unmarshalValidDocument(contextDocument);
    process(unmarshalledDocument);
    return Jaxb.marshalPskcType(documentBuilder, Jaxb.createMarshaller(JAXB_CONTEXT), unmarshalledDocument);
  }

  /**
   * Validates the signature of a given document and returns an unmarshaled {@link KeyContainer} instance.
   *
   * @param contextDocument Document instance
   * @return Unmarshaled instance
   * @throws XMLSignatureException If the signature validation fails
   * @throws JAXBException If the document cannot be unmarshaled
   * @throws MarshalException If the document cannot be marshaled
   */
  public static KeyContainer unmarshalValidDocument(final Document contextDocument)
          throws XMLSignatureException, JAXBException, MarshalException
  {
    //Validate the entire document
    Validator.validateDocument(contextDocument);
    final KeyContainer unmarshalledDocument = Jaxb.createUnmarshaller(JAXB_CONTEXT)
            .unmarshal(contextDocument, KeyContainer.class).getValue();
    return unmarshalledDocument;
  }

  /**
   * Process the contents of a {@link KeyContainer}. The processing depends on the initialization parameters of
   * {@link #init(int, byte[]) init()}.
   *
   * @param context the context <code>KeyContainer</code>.
   * @return the processed <code>KeyContainer</code>.
   * @throws Exception to indicate any exceptional conditions.
   */
  public KeyContainer doFinal(final KeyContainer context)
          throws Exception
  {
    if (null == context || null == key || cipherMode == Integer.MIN_VALUE) {
      throw new XMLEncryptionException("empty",
              "Context, mode or key is unexpectedly null, perhaps init was not called?");
    }
    process(context);
    return context;
  }

  /**
   * Returns a new {@link JAXBContext} instance with {@link KeyContainer} and {@link DerivedKey} registered.
   * @return New {@link JAXBContext}
   */
  private static JAXBContext initializeJaxbContext()
  {
    try {
      return JAXBContext.newInstance(KeyContainer.class, DerivedKey.class);
    } catch (JAXBException ex) {
      LOGGER.error("Could not initalize jaxbContext", ex);
      throw new IllegalStateException("Could not initialize JAXBContext");
    }
  }

  /**
   * Calls the appropriate methods given the initialized mode of the cipher.
   *
   * @param unmarshalledDocument Unmarshalled document instance
   *
   * @throws OperationNotSupportedException If a specified mode is not yet implemented
   * @throws XMLEncryptionException If an unknown mode is specified
   */
  private void process(final KeyContainer unmarshalledDocument) throws OperationNotSupportedException,
          XMLEncryptionException
  {
    switch (cipherMode) {
    case DECRYPT_MODE:
      doDecrypt(unmarshalledDocument);
      break;
    case ENCRYPT_MODE:
      throw new OperationNotSupportedException("This mode has not been implemented yet.");
    default:
      throw new XMLEncryptionException(new IllegalStateException());
    }
  }

  /**
   * Performs decrypt operations for this cipher instance
   *
   * @param context {@link KeyContainer} instance
   */
  private void doDecrypt(final KeyContainer context)
  {
    //Decrypt the marshalled document
    macKey = Decryptor.decryptKeyContainer(context, key, xmlCipher, documentBuilder);
    //Validate the decryption of the document
    Validator.validateKeyPackages(context, macKey);
    //Clean up encryption elements
    Decryptor.cleanup(context);
  }

  /**
   * Handles all validation operations
   */
  static class Validator
  {

    /**
     * Validates document signature, then removes the signature after successful validation
     *
     * @param document Validates a signature node if it exists in the provided Document
     * @throws javax.xml.crypto.MarshalException If the signature node cannot be marshalled
     * @throws javax.xml.crypto.dsig.XMLSignatureException If the signature or the document core is invalid
     * @see #validateSignature(org.w3c.dom.Node)
     */
    public static void validateDocument(final Document document) throws MarshalException, XMLSignatureException
    {
      final Node signatureNode = Xml.getSignatureNodeFromDocument(document);
      if (signatureNode != null) {
        Xml.validateSignature(signatureNode);
        Xml.removeElement((Element) signatureNode);
      }
    }

    /**
     * Iterates through and validates {@link KeyPackageType} child elements using their ValueMAC element and the
     * provided macKey. ValueMAC element of Secrets are nulled after successful validation.
     *
     * @param marshalled {@link KeyContainer} instance
     * @param macKey The decrypted MACKey
     * @throws RuntimeException If the underlying validation cannot be performed
     */
    public static void validateKeyPackages(final KeyContainer marshalled, final byte[] macKey)
    {
      Jaxb.forEveryKeyPackageWithEncryptedValue(marshalled, (encryptedValue, keyPackage) -> {
        if (marshalled.getMACMethod() != null) {
          try {
            //Validate decryption if a MACMethod exists
            Validator.validateMAC(marshalled.getMACMethod().getAlgorithm(),
                    encryptedValue.getCipherData().getCipherValue(),
                    keyPackage.getKey().getData().getSecret().getValueMAC(),
                    macKey);
          } catch (NoSuchAlgorithmException | InvalidKeyException | PskcCryptoException ex) {
            throw new RuntimeException(ex);
          }
        }
      });
    }

    /**
     * Validates the encrypted values based on the MACMethod described in the PSKC document.
     *
     * @param algorithm Decrypted MACMethod algorithm
     * @param cipherData The {@link EncryptedDataType} cipher value
     * @param valueMAC cipher data as described by PSKC schema
     * @param macKey The decrypted MACKey
     * @throws NoSuchAlgorithmException If the MAC method algorithm is not supported.
     * @throws InvalidKeyException If a wrong secretKey is given
     * @throws PskcCryptoException If the validation fails
     */
    public static void validateMAC(final String algorithm, final byte[] cipherData, final byte[] valueMAC,
            final byte[] macKey)
            throws NoSuchAlgorithmException, InvalidKeyException, PskcCryptoException
    {
      final String publicAlgorithm = JCEMapper.translateURItoJCEID(algorithm);
      if (publicAlgorithm == null) {
        throw new NoSuchAlgorithmException(
                "Could not find MAC algorithm " + algorithm + " for validation");
      }
      final Mac mac = Mac.getInstance(publicAlgorithm);
      mac.init(new SecretKeySpec(macKey, publicAlgorithm));
      final byte[] result = mac.doFinal(cipherData);
      if (!Arrays.areEqual(result, valueMAC)) {
        throw new PskcCryptoException("MAC validation failed");
      }
    }

  }

  /**
   * Handles all decryption operations
   */
  static class Decryptor
  {

    /**
     * Default constructor
     */
    Decryptor()
    {
    }

    /**
    * Removes encryption and signature elements after decryption
    *
    * @param unmarshalled Unmarshalled {@link KeyContainer} instance
    */
    public static void cleanup(final KeyContainer unmarshalled)
    {
      unmarshalled.setMACMethod(null);
      unmarshalled.setEncryptionKey(null);
      unmarshalled.setSignature(null);
      Jaxb.forEveryKeyPackageWithEncryptedValue(unmarshalled, (encryptedValue, keyPackage) -> {
        keyPackage.getKey().getData().getSecret().setValueMAC(null);
        keyPackage.getKey().getData().getSecret().setEncryptedValue(null);
      });
    }

    /**
     * Iterates through and decrypts {@link KeyPackageType} child elements using their EncryptedValue element and the
     * provided secretKey. EncryptedValue element of Secrets are replaced with PlainValue after successful decryption.
     *
     * @param marshalled {@link KeyContainer} instance
     * @param secretKey Secret key
     * @param xmlCipher XMLCipher used to decrypt the element
     * @param documentBuilder DocumentBuilder instance to marshal encrypted data into xmlCipher
     */
    public static void decryptKeyPackages(final KeyContainer marshalled, final byte[] secretKey,
            final XMLCipher xmlCipher, final DocumentBuilder documentBuilder)
    {
      Jaxb.forEveryKeyPackageWithEncryptedValue(marshalled, (encryptedValue, keyPackage) -> {
        try {
          //If there is an encrypted value present
          Jaxb.setPlainValue(keyPackage, decryptEncryptedData(encryptedValue, secretKey, xmlCipher, documentBuilder));
        } catch (PskcCryptoException ex) {
          throw new RuntimeException(ex);
        }
      });
    }

    /**
     * Looks for the existence of a {@link DerivedKeyType#DEFAULT_ELEMENT_LOCAL_NAME} within a given a document element
     * and derives the key as described by PSKC schema and PKCS#5 PBKF2 key derivation algorithm. If the element doesn't
     * exist within the given document, then kek is returned as provided.
     *
     * This method removes the element after successful derivation.
     *
     * @param marshalled Element to search within
     * @param kek Key encrypting key
     * @throws PskcCryptoException In case of crypto exceptions
     * @return Derived secret key data, if the element doesn't exist, kek is returned as is.
     */
    public static byte[] decryptPBKF2FromKEK(final KeyContainer marshalled, final byte[] kek)
    {
      final Object foundDerivedKey = Jaxb.getDerivedKey(marshalled);
      if (foundDerivedKey != null) {
        final DerivedKey derivedKeyType = (DerivedKey) foundDerivedKey;
        final KeyDerivationMethod derivationMethod = derivedKeyType.getKeyDerivationMethod();
        if (derivationMethod == null || !Constants.PSKC_ALGO_PBKDF2_URI.equals(derivationMethod.getAlgorithm())) {
          throw new PskcCryptoException("Unsupported key derivation algorithm");
        }
        final PBKDF2Params pbkdf2Parameters = Jaxb.getPBKDF2Parameter(derivationMethod);
        final byte[] derivedKey = Crypto.derivePBKDF2KeyFromKEK(kek,
                pbkdf2Parameters.getPRF() != null &&
                pbkdf2Parameters.getPRF().getAlgorithm() != null ?
                        pbkdf2Parameters.getPRF().getAlgorithm() : null,
                pbkdf2Parameters.getSalt().getSpecified(),
                pbkdf2Parameters.getIterationCount().intValueExact(),
                pbkdf2Parameters.getKeyLength().intValueExact());
        return derivedKey;
      }
      return kek;
    }

    /**
     * Decrypts and sets the macKey for this cipher instance, if a mac key is not found, the initial key supplied is
     * used for mac validation
     *
     * @param marshalled {@link KeyContainer} to look under
     * @param keyBytes Key supplied to decrypt the MACMethodType element
     * @param xmlCipher XMLCipher used to decrypt the element
     * @param documentBuilder DocumentBuilder instance to marshal encrypted data into xmlCipher
     * @return Decrypted key, null if a MACMethod does not exist under the given KeyContainer
     */
    public static byte[] decryptMacMethod(final KeyContainer marshalled, final byte[] keyBytes,
            final XMLCipher xmlCipher, final DocumentBuilder documentBuilder)
    {
      byte[] returnValue = null;
      if (marshalled.getMACMethod() != null && marshalled.getMACMethod().getMACKey() != null) {
        returnValue = decryptEncryptedData(marshalled.getMACMethod().getMACKey(), keyBytes, xmlCipher,
                documentBuilder);
      }
      return returnValue;
    }

    /**
     * Decrypts a given PSKC http://www.w3.org/2009/xmlenc11# EncryptedData definition implementing the standards
     * required by RFC6030.
     *
     * @param encryptedData {@link EncryptedData} element
     * @param keyBytes Key bytes used to either decrypt or unwrap
     * @param xmlCipher XMLCipher used to decrypt the element
     * @param documentBuilder DocumentBuilder instance to marshal encrypted data into xmlCipher
     * @return Decrypted data
     */
    public static byte[] decryptEncryptedData(final EncryptedData encryptedData, final byte[] keyBytes,
            final XMLCipher xmlCipher, final DocumentBuilder documentBuilder)
    {
      try {
        final byte[] decryptedData;
        xmlCipher.init(XMLCipher.DECRYPT_MODE, null);
        final String algoURI = encryptedData.getEncryptionMethod().getAlgorithm();
        final byte[] cipherData = encryptedData.getCipherData().getCipherValue();
        final String publicKeyAlgorithm = JCEMapper.getJCEKeyAlgorithmFromURI(algoURI);
        if (publicKeyAlgorithm == null) {
          throw new PskcCryptoException("No such algorithm");
        }
        final String jceID = JCEMapper.translateURItoJCEID(algoURI);
        //If unwrap instead of decrypt
        final boolean unwrapKey = jceID != null && jceID.endsWith(Constants.WRAPPING);
        if (unwrapKey) {
          decryptedData = unwrapToByteArray(publicKeyAlgorithm, keyBytes, cipherData);
        } else {
          decryptedData = decryptToByteArray(publicKeyAlgorithm, keyBytes, encryptedData, xmlCipher, documentBuilder);
        }
        return decryptedData;
      } catch (InvalidCipherTextException | XMLEncryptionException ex) {
        throw new PskcCryptoException("Could not decrypt EncryptedData", ex);
      }
    }

    /**
     * Decrypts a given {@link EncryptedData} element using the underlying {@link XMLCipher} instance
     *
     * @param publicKeyAlgorithm Algorithm to use
     * @param keyBytes Key to use
     * @param encryptedData {@link EncryptedData} to decrypt
     * @param xmlCipher XMLCipher used to decrypt the element
     * @param documentBuilder DocumentBuilder instance to marshal encrypted data into xmlCipher
     * @return Decrypted bytes
     * @throws XMLEncryptionException If decryption fails
     */
    public static byte[] decryptToByteArray(final String publicKeyAlgorithm, final byte[] keyBytes,
            final EncryptedData encryptedData, final XMLCipher xmlCipher, final DocumentBuilder documentBuilder)
            throws XMLEncryptionException
    {
      final byte[] decryptedData;
      final Key xmlCipherkey;
      if (publicKeyAlgorithm.startsWith(Constants.DES_EDE)) {
        xmlCipherkey = new SecretKeySpec(Crypto.cast3DESnTDEATo3TDEA(keyBytes), publicKeyAlgorithm);
      } else {
        xmlCipherkey = new SecretKeySpec(keyBytes, publicKeyAlgorithm);
      }
      xmlCipher.init(XMLCipher.DECRYPT_MODE, xmlCipherkey);
      decryptedData = xmlCipher.decryptToByteArray(Jaxb.marshalPskcType(documentBuilder,
              Jaxb.createMarshaller(JAXB_CONTEXT), encryptedData).getDocumentElement());
      return decryptedData;
    }

    /**
     * Unwraps a given {@link EncryptedData} element using the {@link Crypto} class
     *
     * @param publicKeyAlgorithm Algorithm to use
     * @param keyBytes Key to unwrap with
     * @param cipherData Data to unwrap
     * @return Unwrapped bytes
     * @throws PskcCryptoException If the algorithm is not supported
     * @throws InvalidCipherTextException If the unwrapping was not successful
     */
    public static byte[] unwrapToByteArray(final String publicKeyAlgorithm, final byte[] keyBytes,
            final byte[] cipherData) throws PskcCryptoException, InvalidCipherTextException
    {
      final byte[] decryptedData;
      if (publicKeyAlgorithm.startsWith(Constants.AES)) {
        decryptedData = Crypto.unwrapAESBlockLengthKey(keyBytes, cipherData);
      } else if (publicKeyAlgorithm.startsWith(Constants.DES_EDE)) {
        decryptedData = Crypto.unwrapDESedeKey(Crypto.cast3DESnTDEATo3TDEA(keyBytes), cipherData);
      } else {
        throw new PskcCryptoException("Unsupported wrapping algorithm: " + publicKeyAlgorithm);
      }
      return decryptedData;
    }

    /**
     * Decrypts a given valid PSKC XML document and returns the decrypted format.
     *
     * @param marshalled Encrypted document to decrypt
     * @param secretKeyData Secret key bytes
     * @param xmlCipher XMLCipher used to decrypt the element
     * @param documentBuilder DocumentBuilder instance to marshal encrypted data into xmlCipher
     * @return The macKey for validation (macKey falls back to secret key if it does not exist)
     */
    public static byte[] decryptKeyContainer(final KeyContainer marshalled, final byte[] secretKeyData,
            final XMLCipher xmlCipher, final DocumentBuilder documentBuilder)
    {
      final byte[] secretKey = Decryptor.decryptPBKF2FromKEK(marshalled, secretKeyData);
      final byte[] decryptedMacKey = Decryptor.decryptMacMethod(marshalled, secretKey, xmlCipher, documentBuilder);
      final byte[] macKey = decryptedMacKey != null ? decryptedMacKey : secretKey;
      Decryptor.decryptKeyPackages(marshalled, secretKey, xmlCipher, documentBuilder);
      return macKey;
    }
  }
}
