/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.pskccrypt;

/**
 * Runtime error describing a generic cryptographic problem.
 *
 * @author Middleware Services
 */
public class PskcCryptoException extends RuntimeException
{

  /**
   * serialVersionUID
   */
  private static final long serialVersionUID = 1286485012718913626L;

  /**
   * Creates a new instance with the given error message.
   *
   * @param message Error message.
   */
  public PskcCryptoException(final String message)
  {
    super(message);
  }

  /**
   * Creates a new instance with the given error message and cause.
   *
   * @param message Error message.
   * @param cause Error cause.
   */
  public PskcCryptoException(final String message, final Throwable cause)
  {
    super(message, cause);
  }

}
