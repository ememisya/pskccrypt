/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.pskccrypt.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.Iterator;
import java.util.Properties;
import javax.xml.crypto.AlgorithmMethod;
import javax.xml.crypto.KeySelector;
import javax.xml.crypto.KeySelectorException;
import javax.xml.crypto.KeySelectorResult;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import edu.vt.middleware.pskccrypt.Constants;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * General XML utilities used in this library.
 *
 * @author Middleware Services
 */
public final class Xml
{

  /**
   * Private constructor of utility class.
   */
  private Xml()
  {
  }

  /**
   * Searches for the first occurrence of an element in a given namespace and nodeName.
   *
   * @param element Element to search within
   * @param namespace Namespace
   * @param nodeName Local name of the node
   * @return Found {@link Element}, otherwise <b>null</b> is returned.
   */
  public static Element getFirstElementInElementByNS(final Element element,
          final String namespace, final String nodeName)
  {
    final NodeList derivedKeys = element.getElementsByTagNameNS(namespace, nodeName);
    if (derivedKeys != null && derivedKeys.getLength() > 0) {
      return (Element) derivedKeys.item(0);
    }
    return null;
  }

  /**
   * Creates a new {@link DocumentBuilder} to handle {@link Document} instances ensuring the namespaces
   * get preserved.
   *
   * @return new {@link DocumentBuilder}
   * @throws ParserConfigurationException In case of initialization exceptions
   */
  public static DocumentBuilder newNamespaceAwareDocumentBuilder() throws ParserConfigurationException
  {
    final DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    dbFactory.setNamespaceAware(true);
    return dbFactory.newDocumentBuilder();
  }

  /**
   * Removes a given child element from its parent.
   *
   * @param element Element to remove
   */
  public static void removeElement(final Element element)
  {
    element.getParentNode().removeChild(element);
  }

  /**
   * Returns the signature node in a given Document, null if it doesn't exist.
   *
   * @param docRoot Document to traverse
   * @return Found signature node, null otherwise
   */
  public static Node getSignatureNodeFromDocument(final Document docRoot)
  {
    final Node signatureNode = Xml.getFirstElementInElementByNS(
            docRoot.getDocumentElement(), XMLSignature.XMLNS, Constants.PSKC_TAG_SIGNATURE);
    return signatureNode;
  }

  /**
   * Validates the Signature node provided in a given Document
   *
   * @param signatureNode Node to validate as Signature
   * @throws javax.xml.crypto.MarshalException If the signature node cannot be marshalled
   * @throws javax.xml.crypto.dsig.XMLSignatureException If the signature or the document core is invalid
   */
  public static void validateSignature(final Node signatureNode) throws MarshalException, XMLSignatureException
  {
    if (signatureNode == null) {
      throw new IllegalArgumentException("Node cannot be null");
    }
    try {
      final XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");
      final DOMValidateContext valContext = new DOMValidateContext(new X509KeySelector(), signatureNode);
      final XMLSignature signature = fac.unmarshalXMLSignature(valContext);
      final boolean coreValidity = signature.validate(valContext);
      if (!coreValidity) {
        throw new XMLSignatureException("Signature verification for document core validity failed");
      }
    } catch (MarshalException | XMLSignatureException ex) {
      throw ex;
    }
  }

  /**
   * Replaces a given element with another element.
   *
   * @param element Element to be replaced
   * @param replaceWith Element to substitute for
   */
  public static void replaceElement(final Element element, final Element replaceWith)
  {
    if (element.getOwnerDocument().equals(replaceWith.getOwnerDocument())) {
      element.getParentNode().replaceChild(replaceWith, element);
    }
  }

  /**
   * Writes a given {@link Document} to file.
   *
   * @param document {@link Document} to write
   * @param file {@link File#File(java.lang.String)}
   * @param properties {@link Transformer} output properties, may be null
   * @throws javax.xml.transform.TransformerException If the document could not be formatted as XML
   * @throws java.io.IOException In case of IO exceptions
   */
  public static void writeDocument(final Document document, final File file, final Properties properties)
          throws IOException, TransformerException
  {
    try (FileOutputStream f = new FileOutputStream(file)) {
      final TransformerFactory factory = TransformerFactory.newInstance();
      final Transformer transformer = factory.newTransformer();
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      final DOMSource source = new DOMSource(document);
      final StreamResult result = new StreamResult(f);
      if (properties != null) {
        transformer.setOutputProperties(properties);
      }
      transformer.transform(source, result);
    }
  }

  /**
   * Writes a given {@link Document} to file.
   *
   * @param document {@link Document} to write
   * @param filePath Abstract pathname to the XML document. See: {@link File#File(java.lang.String)}
   * @throws javax.xml.transform.TransformerException If the document could not be formatted as XML
   * @throws java.io.IOException In case of IO exceptions
   */
  public static void writeDocument(final Document document, final String filePath)
          throws IOException, TransformerException
  {
    writeDocument(document, new File(filePath), null);
  }

  /**
   * Writes a given {@link Document} to file. If the {@link Properties} parameter is null, this method equates to
   * calling {@link #writeDocument(org.w3c.dom.Document, java.lang.String)}.
   *
   * @param document {@link Document} to write
   * @param properties {@link Transformer} output properties, may be null
   * @param filePath Abstract pathname to the XML document. See: {@link File#File(java.lang.String)}
   * @throws javax.xml.transform.TransformerException If the document could not be formatted as XML
   * @throws java.io.IOException In case of IO exceptions
   */
  public static void writeDocument(final Document document, final String filePath, final Properties properties)
          throws IOException, TransformerException
  {
    writeDocument(document, new File(filePath), properties);
  }

  /**
   * {@link KeySelector} extension which looks for X509Data element under the signature
   */
  static class X509KeySelector extends KeySelector
  {

    /**
     * Default constructor
     */
    X509KeySelector()
    {
    }

    @Override
    public KeySelectorResult select(final KeyInfo keyInfo,
            final KeySelector.Purpose purpose,
            final AlgorithmMethod method,
            final XMLCryptoContext context)
            throws KeySelectorException
    {
      final Iterator ki = keyInfo.getContent().iterator();
      while (ki.hasNext()) {
        final XMLStructure info = (XMLStructure) ki.next();
        if (!(info instanceof X509Data)) {
          continue;
        }
        final X509Data x509Data = (X509Data) info;
        final Iterator xi = x509Data.getContent().iterator();
        while (xi.hasNext()) {
          final Object o = xi.next();
          if (!(o instanceof X509Certificate)) {
            continue;
          }
          final PublicKey key = ((X509Certificate) o).getPublicKey();
          final String methodAlgorithm = method.getAlgorithm();
          final String keyAlgorithm = key.getAlgorithm();
          if (("DSA".equalsIgnoreCase(keyAlgorithm) &&
                  methodAlgorithm.equalsIgnoreCase(SignatureMethod.DSA_SHA1)) ||
                  ("RSA".equalsIgnoreCase(keyAlgorithm) &&
                  methodAlgorithm.equalsIgnoreCase(SignatureMethod.RSA_SHA1))) {
            return () -> key;
          }
        }
      }
      throw new KeySelectorException("Could not find a supported key selection mechanism for signature");
    }
  }
}
