/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.pskccrypt.util;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import com.sun.xml.bind.marshaller.NamespacePrefixMapper;
import edu.vt.middleware.pskccrypt.PskcCryptoException;
import ietf.params.xml.ns.keyprov.pskc.BinaryDataType;
import ietf.params.xml.ns.keyprov.pskc.KeyContainer;
import ietf.params.xml.ns.keyprov.pskc.KeyPackageType;
import org.w3._2001._04.xmlenc_.EncryptedData;
import org.w3._2009.xmlenc11_.DerivedKey;
import org.w3._2009.xmlenc11_.KeyDerivationMethod;
import org.w3._2009.xmlenc11_.PBKDF2Params;
import org.w3c.dom.Document;

/**
 * General XML utilities used in this library.
 *
 * @author Middleware Services
 */
public final class Jaxb
{

  /**
   * Private constructor of utility class.
   */
  private Jaxb()
  {
  }

  /**
   * Iterates through an object and returns all found instances of a given class
   *
   * @param <T> Class to search and return instances of
   * @param any List to iterate over
   * @param clazz Types to find and return
   * @return List of found types, (never null)
   */
  public static <T> List<T> findAllFromAny(final List<Object> any, final Class<T> clazz)
  {
    final List<T> foundElements = new ArrayList<>();
    if (any != null && !any.isEmpty()) {
      any.stream().filter(object -> clazz.isAssignableFrom(object.getClass())).forEach(object -> {
        foundElements.add(clazz.cast(object));
      });
    }
    return foundElements;
  }

  /**
   * Iterates through an object until a given class type is found
   *
   * @param <T> Class to search and return instances of
   * @param any List to iterate over
   * @param clazz Type to find and return
   * @return Found object cast to given class, null otherwise
   */
  public static <T> T findTypeFromAny(final List<Object> any, final Class<T> clazz)
  {
    final Object foundObject = findObjectFromAny(any, clazz);
    if (foundObject != null) {
      return clazz.cast(foundObject);
    }
    return null;
  }

  /**
   * Iterates through an object until a given class type is found
   *
   * @param any List to iterate over
   * @param clazz Type to find and return as Object
   * @return Found Object, null otherwise
   */
  @SuppressWarnings("unchecked")
  public static Object findObjectFromAny(final List<Object> any, final Class clazz)
  {
    if (any != null && !any.isEmpty()) {
      for (Object object : any) {
        if (clazz.isAssignableFrom(object.getClass())) {
          return object;
        }
      }
    }
    return null;
  }

  /**
   * Returns a new {@link BinaryDataType}
   *
   * @param derivedKey plain value data
   * @return {@link BinaryDataType} containing <b>PlainValue</b>
   */
  public static BinaryDataType newPlainValue(final byte[] derivedKey)
  {
    final BinaryDataType decryptedType = new BinaryDataType();
    decryptedType.setPlainValue(derivedKey);
    return decryptedType;
  }

  /**
   * Replaces an encrypted value with plain value in a given marshalled structure
   *
   * @param keyPackage Key package to replace in
   * @param plainValue Value to be marshalled as the plain value (base64)
   */
  public static void setPlainValue(final KeyPackageType keyPackage, final byte[] plainValue)
  {
    keyPackage.getKey().getData().getSecret().setPlainValue(plainValue);
  }

  /**
   * Returns the {@link PBKDF2Params} of a specified {@link KeyDerivationMethod}
   *
   * @param derivationMethod {@link KeyDerivationMethod} to look under
   * @return Found {@link PBKDF2Params}
   * @throws PskcCryptoException If {@link PBKDF2Params} does not exist
   */
  public static PBKDF2Params getPBKDF2Parameter(final KeyDerivationMethod derivationMethod) throws PskcCryptoException
  {
    final PBKDF2Params found = Jaxb.findTypeFromAny(derivationMethod.getAnies(), PBKDF2Params.class);
    if (found != null) {
      return found;
    }
    throw new PskcCryptoException("Malformed PBKDF2Parameter element");
  }

  /**
   * Marshals a PSKC root element type into a new {@link Document} under the jaxb context statically defined in this
   * library.
   *
   * @param <T> Type of the object
   * @param type Object of the given class to marshal
   * @param jaxbMarshaller {@link Marshaller} instance
   * @param documentBuilder {@link DocumentBuilder} instance
   * @return Marshalled {@link Document} instance.
   * @throws IllegalArgumentException If the document could not be marshalled in the preset context
   */
  public static <T> Document marshalPskcType(final DocumentBuilder documentBuilder, final Marshaller jaxbMarshaller,
          final T type) throws IllegalArgumentException
  {
    final Document encryptedDoc = documentBuilder.newDocument();
    if (type != null) {
      try {
        jaxbMarshaller.marshal(type, encryptedDoc);
      } catch (JAXBException ex) {
        throw new IllegalArgumentException("Could not marshal encrypted document", ex);
      }
    }
    return encryptedDoc;
  }

  /**
   * Method to run a {@link BiConsumer} for every {@link KeyPackageType} found in a {@link KeyContainer} and
   * the {@link EncryptedData} from EncryptedValue element
   *
   * @param marshalled {@link KeyContainer} instance
   * @param restrictParameters {@link KeyContainer} to run for every {@link KeyPackageType}
   */
  public static void forEveryKeyPackageWithEncryptedValue(final KeyContainer marshalled,
          final BiConsumer<EncryptedData, KeyPackageType> restrictParameters)
  {
    if (marshalled.getKeyPackages() == null) {
      //Nothing to do
      return;
    }
    for (int i = 0; i < marshalled.getKeyPackages().size(); i++) {
      final KeyPackageType keyPackage = marshalled.getKeyPackages().get(i);
      final EncryptedData encryptedValue = Jaxb.getEncryptedValue(keyPackage);
      restrictParameters.accept(encryptedValue, keyPackage);
    }
  }

  /**
   * Returns the {@link Object} reference of a {@link DerivedKey}, under the specified {@link KeyContainer}
   *
   * @param marshalled {@link KeyContainer} to look under
   * @return Found {@link Object} reference of {@link DerivedKey}, null otherwise
   */
  public static Object getDerivedKey(final KeyContainer marshalled)
  {
    if (marshalled.getEncryptionKey() != null && marshalled.getEncryptionKey().getContent() != null) {
      return Jaxb.findObjectFromAny(marshalled.getEncryptionKey().getContent(), DerivedKey.class);
    }
    return null;
  }

  /**
   * Returns an {@link EncryptedData} out of a key package if it exists, returns null otherwise.
   *
   * @param keyPackage {@link KeyPackageType} to traverse
   * @return Found {@link EncryptedData}, null otherwise
   */
  public static EncryptedData getEncryptedValue(final KeyPackageType keyPackage)
  {
    if (keyPackage.getKey() != null && keyPackage.getKey().getData() != null &&
            keyPackage.getKey().getData().getSecret() != null &&
            keyPackage.getKey().getData().getSecret().getEncryptedValue() != null) {
      return keyPackage.getKey().getData().getSecret().getEncryptedValue();
    }
    return null;
  }

  /**
   * Creates the {@link Unmarshaller} used in the PSKC cipher context
   *
   * @param jaxbContext JAXB context
   * @return New instance
   */
  public static Unmarshaller createUnmarshaller(final JAXBContext jaxbContext)
  {
    try {
      return jaxbContext.createUnmarshaller();
    } catch (JAXBException jAXBException) {
      throw new PskcCryptoException("Could not create marshaller", jAXBException);
    }
  }

  /**
   * Creates the {@link Marshaller} used in the pskc cipher context
   *
   * @param jaxbContext JAXB context
   * @return New instance
   */
  public static Marshaller createMarshaller(final JAXBContext jaxbContext)
  {
    Marshaller jaxbMarshaller = null;
    try {
      jaxbMarshaller = jaxbContext.createMarshaller();
      jaxbMarshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NamespaceMapper());
    } catch (JAXBException jAXBException) {
      throw new PskcCryptoException("Could not create marshaller", jAXBException);
    }
    return jaxbMarshaller;
  }

  /**
   * Maps namespace URIs to prefixes defined under RFC6030, if no match is found an empty string will be used.
   */
  static class NamespaceMapper extends NamespacePrefixMapper
  {

    @Override
    public String getPreferredPrefix(final String namespaceUri, final String suggestion, final boolean requirePrefix)
    {
      final String namespace;
      switch (namespaceUri) {
      case "http://www.w3.org/2009/xmlenc11#":
        namespace = "xenc11";
        break;
      case "http://www.rsasecurity.com/rsalabs/pkcs/schemas/pkcs-5v2-0#":
        namespace = "pkcs5";
        break;
      case "urn:ietf:params:xml:ns:keyprov:pskc":
        namespace = "pskc";
        break;
      case "http://www.w3.org/2001/04/xmlenc#":
        namespace = "xenc";
        break;
      case "http://www.w3.org/2000/09/xmldsig#":
        namespace = "ds";
        break;
      default:
        namespace = "";
      }
      return namespace;
    }
  }

}
