/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.pskccrypt;

/**
 * PSKC related and other constants.
 *
 * @author Middleware Services
 */
public final class Constants
{

  /**
   * The encoding used within this library.
   */
  public static final String GLOBAL_ENCODING = "UTF-8";

  /**
   * JCE mapping name for AES algorithm.
   */
  public static final String AES = "AES";

  /**
   * JCE mapping name for 3DES algorithm.
   */
  public static final String DES_EDE = "DESede";

  /**
   * JCE mapping suffix for key wrapping algorithms.
   */
  public static final String WRAPPING = "Wrap";

  /**
   * JCE mapping for default PBKDF2_PRF.
   */
  public static final String DEFAULT_PBKDF2_PRF = "HmacSHA1";

  /**
   * PSKC tag.
   */
  public static final String PSKC_NS_TAG = "pskc";

  /**
   * PSKC name space.
   */
  public static final String PSKC_NS = "urn:ietf:params:xml:ns:keyprov:pskc";

  /**
   * PKCS#5 name space.
   */
  public static final String PKCS5V2_NS = "http://www.rsasecurity.com/rsalabs/pkcs/schemas/pkcs-5v2-0#";

  /**
   * pbkdf2 algorithm URI
   */
  public static final String PSKC_ALGO_PBKDF2_URI = PKCS5V2_NS + "pbkdf2";

  /**
   * Local name as per PSKC schema for the dsig Element Signature
   */
  public static final String PSKC_TAG_SIGNATURE = "Signature";

  /**
   * Private constructor of utility class.
   */
  private Constants()
  {
  }

}
