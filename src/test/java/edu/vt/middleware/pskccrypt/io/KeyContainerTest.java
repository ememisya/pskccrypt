/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.pskccrypt.io;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import edu.vt.middleware.pskccrypt.Constants;
import edu.vt.middleware.pskccrypt.FailListener;
import edu.vt.middleware.pskccrypt.PskcCipher;
import edu.vt.middleware.pskccrypt.util.Jaxb;
import edu.vt.middleware.pskccrypt.util.Xml;
import ietf.params.xml.ns.keyprov.pskc.KeyContainer;
import org.cryptacular.util.CodecUtil;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 * Tests various types of PSKC files for proper decryption
 *
 * @author Middleware Services
 */
@Listeners(FailListener.class)
public class KeyContainerTest
{

  private static final String RESOURCES_PATH = "src/test/resources/";

  private static final String ADDITIONAL_ALGORITHM_PROFILE_PATH = "draft-hoyer-keyprov-pskc-algorithm-profiles-01/";
  private static final String ENCRYPTED_PATH = "encryption/";
  private static final String FEITIAN_PATH = "feitian/";
  private static final String INVALID_PATH = "invalid/";
  private static final String MISCELLANEOUS_PATH = "misc/";
  private static final String NAMESPACE_PATH = "namespace/";
  private static final String SIGNATURE_PATH = "signature/";
  private static final String NAGRAID_PATH = "nagraid/";

  @DataProvider(name = "additionalAlgorithmProfileKeyContainers")
  public Object[][] getAdditionalAlgorithmProfiles()
  {
    return new Object[][]{
      {RESOURCES_PATH + ADDITIONAL_ALGORITHM_PROFILE_PATH + "actividentity-3des.pskcxml",},
      {RESOURCES_PATH + ADDITIONAL_ALGORITHM_PROFILE_PATH + "ocra.pskcxml",},
      {RESOURCES_PATH + ADDITIONAL_ALGORITHM_PROFILE_PATH + "securid-aes-counter.pskcxml",},
      {RESOURCES_PATH + ADDITIONAL_ALGORITHM_PROFILE_PATH + "totp.pskcxml",},};
  }

  @DataProvider(name = "encryptedKeyContainers")
  public Object[][] getEncryptedKeyContainers()
  {
    return new Object[][]{
      {RESOURCES_PATH + ENCRYPTED_PATH + "aes128-cbc-noiv.pskcxml",
        "12345678901234567890123456789012", "37383930",},
      {RESOURCES_PATH + ENCRYPTED_PATH + "aes128-cbc.pskcxml",
        "12345678901234567890123456789012", "3132333435363738393031323334353637383930",},
      {RESOURCES_PATH + ENCRYPTED_PATH + "aes192-cbc.pskcxml",
        "123456789012345678901234567890123456789012345678", "3132333435363738393031323334353637383930",},
      {RESOURCES_PATH + ENCRYPTED_PATH + "aes256-cbc.pskcxml",
        "1234567890123456789012345678901234567890123456789012345678901234", "3132333435363738393031323334353637383930",},
      {RESOURCES_PATH + ENCRYPTED_PATH + "kw-aes128.pskcxml",
        "000102030405060708090A0B0C0D0E0F", "00112233445566778899AABBCCDDEEFF",},
      {RESOURCES_PATH + ENCRYPTED_PATH + "kw-aes192.pskcxml",
        "000102030405060708090A0B0C0D0E0F1011121314151617", "00112233445566778899AABBCCDDEEFF",},
      {RESOURCES_PATH + ENCRYPTED_PATH + "kw-aes256.pskcxml",
        "000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F", "00112233445566778899AABBCCDDEEFF0001020304050607",},
      {RESOURCES_PATH + ENCRYPTED_PATH + "kw-tripledes.pskcxml",
        "255e0d1c07b646dfb3134cc843ba8aa71f025b7c0838251f", "2923bf85e06dd6ae529149f1f1bae9eab3a7da3d860d3e98",},
      {RESOURCES_PATH + ENCRYPTED_PATH + "no-mac-key.pskcxml",
        "12345678901234567890123456789012", "3132333435363738393031323334353637383930",},
      {RESOURCES_PATH + ENCRYPTED_PATH + "tripledes-cbc.pskcxml",
        "12345678901234567890123456789012", "3132333435363738393031323334353637383930",},};
  }

  @DataProvider(name = "signedKeyContainers")
  public Object[][] getSignedKeyContainers()
  {
    return new Object[][]{
      {RESOURCES_PATH + SIGNATURE_PATH + "fullyqualifiedns_signed.xml",
        "3FCA3158035072D6".getBytes(), "09fbecfd0bf47910839e2eb05ffa10b95cd0390950ce32ab790583ed134171e0",},};
  }

  @Test(dataProvider = "signedKeyContainers")
  public void testSignatureValidationForKeyContainers(final String keyContainerPath, final byte[] password,
          final String plainText)
          throws Exception
  {
    Document document = readXmlFile(keyContainerPath);
    final PskcCipher cipher = PskcCipher.getInstance();
    cipher.init(PskcCipher.DECRYPT_MODE, password);
    document = cipher.doFinal(document);
    final Element plainElement = Xml.getFirstElementInElementByNS(
            document.getDocumentElement(), Constants.PSKC_NS, "PlainValue");
    Assert.assertNotNull(plainElement);
    Assert.assertEquals(plainElement.getTextContent(), hexToB64(plainText));
  }

  @Test(dataProvider = "signedKeyContainers")
  public void testUnmashalledKeyContainerDecryption(final String keyContainerPath, final byte[] password,
          final String plainText)
          throws Exception
  {
    KeyContainer document = PskcCipher.unmarshalValidDocument(readXmlFile(keyContainerPath));
    final PskcCipher cipher = PskcCipher.getInstance();
    cipher.init(PskcCipher.DECRYPT_MODE, password);
    document = cipher.doFinal(document);
    Assert.assertNotNull(document.getKeyPackages().get(0));
    Assert.assertEquals(CodecUtil.b64(document.getKeyPackages().get(0).getKey().getData().getSecret().getPlainValue()),
            hexToB64(plainText));
  }

  @Test(dataProvider = "encryptedKeyContainers")
  public void testKeyContainerDecryption(final String keyContainerPath, final String password, final String plainText)
          throws Exception
  {
    Document document = readXmlFile(keyContainerPath);
    final PskcCipher cipher = PskcCipher.getInstance();
    cipher.init(PskcCipher.DECRYPT_MODE, CodecUtil.hex(password));
    document = cipher.doFinal(document);
    final Element plainElement = Xml.getFirstElementInElementByNS(
            document.getDocumentElement(), Constants.PSKC_NS, "PlainValue");
    Assert.assertNotNull(plainElement);
    Assert.assertEquals(plainElement.getTextContent(), hexToB64(plainText));
  }

  private Document readXmlFile(final String keyContainerPath) throws SAXException, IOException, ParserConfigurationException
  {
    final File fXmlFile = new File(keyContainerPath);
    final Document doc = Xml.newNamespaceAwareDocumentBuilder().parse(fXmlFile);
    return doc;
  }

  /**
   * Converts a HEX String to Base64Encoded String data.
   *
   * @param hex String encoded as hex
   * @return Base64Encoded
   */
  private String hexToB64(final String hex)
  {
    return CodecUtil.b64(CodecUtil.hex(hex));
  }

}
